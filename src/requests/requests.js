import Authorization from "../pages/Login/Authorization";

export default class API {
  constructor() {
    this.headers = this.getHeaders();
  }

  getHeaders() {
    let Auth = new Authorization();
    let authHeader = new Headers();

    authHeader.append("access-token", Auth.getToken().access_token);
    authHeader.append("Content-Type", "application/json");
    authHeader.append("client", Auth.getToken().client);
    authHeader.append("uid", Auth.getToken().uid);

    return {
      method: "GET",
      headers: authHeader
    };
  }

  signIn = data => {
    let authHeader = new Headers();
    authHeader.append("Content-Type", "application/json");
    return fetch(`${process.env.REACT_APP_API}/users/auth/sign_in`, {
      method: "POST",
      headers: authHeader,
      body: JSON.stringify(data)
    });
  };

  getEmpresas(id = null) {
    return fetch(
      `${process.env.REACT_APP_API}/enterprises/` + (id ? id : ""),
      this.headers
    );
  }
}
