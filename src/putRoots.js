import React from "react";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import purple from "@material-ui/core/colors/purple";

const theme = createMuiTheme({
  palette: {
    primary: {
      light: purple[300],
      main: purple[500],
      dark: purple[700]
    },
    secondary: {
      light: "#78c8c9",
      main: "#57bbbc",
      dark: "#3c8283"
    }
  },
  typography: {
    useNextVariants: true
  }
});

function putRoots(Component) {
  function putRoots(props) {
    return (
      <MuiThemeProvider theme={theme}>
        <Component {...props} />
      </MuiThemeProvider>
    );
  }

  return putRoots;
}

export default putRoots;
