import React, { Component } from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import "./App.scss";

import { Login, LayoutTemp } from "./pages";

import { routes } from "./routes";

import Authorization from "./pages/Login/Authorization";

const Auth = new Authorization();

const LoginRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      !(Auth.loggedIn() === true) ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/"
          }}
        />
      )
    }
  />
);

const ProtectedRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      Auth.loggedIn() === false ? (
        <Component {...props} routes={routes} />
      ) : (
        <Redirect
          to={{
            pathname: "/login"
          }}
        />
      )
    }
  />
);

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <LoginRoute exact path="/login" component={Login} />
          <ProtectedRoute path="/" component={LayoutTemp} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
