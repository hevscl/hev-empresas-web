import React from "react";
import classNames from "classnames";

import { Typography } from "@material-ui/core";

import styles from "./ErrorPage.module.scss";

class ErrorPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <main className={classNames(styles.ErrorPage, "container")}>
        <Typography component="h1" variant="h4" gutterBottom>
          Página não encontrada
        </Typography>
      </main>
    );
  }
}

export default ErrorPage;
