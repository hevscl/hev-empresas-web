import React, { Suspense } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import PropTypes from "prop-types";
import classNames from "classnames";

import { withStyles } from "@material-ui/core/styles";
import putRoots from "../../putRoots";

const styles = theme => ({
  root: {
  }
});

class LayoutTemp extends React.Component {
  render() {
    const { classes, routes } = this.props;

    return (
      <div className={classNames(classes.root)}>
        <BrowserRouter>
          <Suspense fallback={<main className="main">Carregando...</main>}>
            <Switch>
              {routes.map((route, index) => {
                return route.component ? (
                  <Route
                    key={index}
                    path={route.path}
                    exact={route.exact}
                    render={props => <route.component {...props} />}
                  />
                ) : null;
              })}
            </Switch>
          </Suspense>
        </BrowserRouter>
      </div>
    );
  }
}

LayoutTemp.propTypes = {
  classes: PropTypes.object.isRequired
};

export default putRoots(withStyles(styles)(LayoutTemp));
