import Login from "./Login";
import LayoutTemp from "./LayoutTemp";
import Home from "./Home";
import ErrorPage from "./Error";

export { Login, LayoutTemp, Home, ErrorPage };
