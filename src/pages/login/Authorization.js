import API from "../../requests/requests";

export default class Authorization {
  constructor() {}

  signIn(data) {
    return new API().signIn(data);
  }

  setToken(data) {
    localStorage.setItem("empresas_access_token", data.access_token);
    localStorage.setItem("empresas_client", data.client);
    localStorage.setItem("empresas_uid", data.uid);
  }

  getToken() {
    return {
      access_token: localStorage.getItem("empresas_access_token"),
      client: localStorage.getItem("empresas_client"),
      uid: localStorage.getItem("empresas_uid")
    };
  }

  loggedIn() {
    return !!this.getToken().access_token;
  }

  logout() {
    localStorage.removeItem("empresas_access_token");
    localStorage.removeItem("empresas_client");
    localStorage.removeItem("empresas_uid");
  }
}
