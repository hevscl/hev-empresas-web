import React from "react";
import classNames from "classnames";

import {
  Button,
  Typography,
  InputAdornment,
  Grid,
  TextField,
  CircularProgress,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions
} from "@material-ui/core";

import { EmailOutlined, LockOpenOutlined } from "@material-ui/icons";

import styles from "./Login.module.scss";
import logo from "./../../assets/logo@3x.png";

import Authorization from "./Authorization";

class Login extends React.Component { 
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      alert: "",

      emailError: false,
      passwordError: false,

      processing: false
    };
  }

  componentDidMount() {}

  handleChange = e => {
    const { name, value } = e.target;

    this.setState(() => ({
      [name]: value
    }));

    if (name == "email") this.setState({ emailError: false });
    if (name == "password") this.setState({ passwordError: false });
  };

  handleSubmit = () => {
    let isValid = true;


    
    if (!this.state.email) {
      this.setState({ emailError: true });
      isValid = false;
    } else if (
      !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.state.email)
    ) {
      this.setState({ emailError: true });
      isValid = false;
    }

    if (!this.state.password) {
      this.setState({ passwordError: true });
      isValid = false;
    }

    if (isValid) {
      
      /*
    let data = {
      email: "testeapple@ioasys.com.br",
      password: "12341234"
    };*/

      
      let data = {
        email: this.state.email,
        password: this.state.password
      };

      let Auth = new Authorization();

      this.setState({ processing: true });

      window.location = "/";

      Auth.signIn(data).then(
        function(response) {
          this.setState({ processing: false });

          let contentType = response.headers.get("Content-Type");

          if (contentType && contentType.indexOf("application/json") !== -1) {
            return response.json().then(
              function(json) {
                if (json.success === true) {
                  Auth.setToken({
                    access_token: response.headers.get("access-token"),
                    client: response.headers.get("client"),
                    uid: response.headers.get("uid")
                  });
                  window.location = "/";
                } else {
                  this.setState({ alert: "Verifique seus dados e tente novamente" });
                }
              }.bind(this)
            );
          } else {
            this.setState({
              alert: "Parece que estamos fora do ar"
            });
          }
        }.bind(this)
      );
    }
  };

  handleClose = () => {
    this.setState({ alert: "" });
  };

  render() {
    const { processing } = this.state;

    return (
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        className={styles.Login}
      >
        <Dialog
          open={!processing && !!this.state.alert}
          onClose={this.handleClose}
          aria-describedby="alert-dialog-description"
        >
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {this.state.alert}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary" autoFocus>
              OK
            </Button>
          </DialogActions>
        </Dialog>
        <Grid item className={classNames("container", styles.container)}>
          <img src={logo} alt="Logo ioasys" className={styles.logo} />
          <Typography
            component="h1"
            variant="h5"
            gutterBottom
            className={styles.title}
          >
            BEM VINDO AO <br />
            EMPRESAS
          </Typography>
          <Typography
            component="h2"
            variant="subtitle1"
            gutterBottom
            className={styles.subtitle}
          >
            Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
          </Typography>

          <div className={styles.form}>
            <TextField
              id="email"
              name="email"
              type="email"
              placeholder="E-mail"
              autoComplete="Off"
              error={this.state.emailError}
              fullWidth={true}
              className={styles.inputField}
              onChange={this.handleChange}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <EmailOutlined />
                  </InputAdornment>
                )
              }}
            />

            <TextField
              id="password"
              name="password"
              type="password"
              placeholder="Senha"
              autoComplete="Off"
              error={this.state.passwordError}
              fullWidth={true}
              className={styles.inputField}
              onChange={this.handleChange}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <LockOpenOutlined />
                  </InputAdornment>
                )
              }}
            />
          </div>

          <div className={styles.wrapper}>
            <Button
              variant="contained"
              color="primary"
              fullWidth={true}
              className={styles.button}
              disabled={processing}
              onClick={this.handleSubmit}
            >
              Entrar
            </Button>
            {processing && (
              <CircularProgress size={24} className={styles.buttonProgress} />
            )}
          </div>
        </Grid>
      </Grid>
    );
  }
}

export default Login;
