import React, { useState, useEffect } from "react";
import classNames from "classnames";

import { Card } from "@material-ui/core";

import styles from "./CardEmpresa.module.scss";
import enterprises from "./../../assets/enterprise.svg";

function CardEmpresa(props) {
  const { enterprise, detailView, index, handleClick } = props;

  return (
    <Card
      className={classNames(
        styles.CardEmpresa,
        detailView ? styles.detailView : ""
      )}
      key={index}
      onClick={handleClick}
    >
      <div className={styles.imagem}>
        <img src={enterprises} alt={enterprise.enterprise_name} />
      </div>
      <div className={styles.informacoes}>
        <strong className={styles.nome}>{enterprise.enterprise_name}</strong>
        <span className={styles.negocio}>
          {enterprise.enterprise_type.enterprise_type_name}
        </span>
        <span className={styles.localizacao}>{enterprise.country}</span>
      </div>
      <div className={styles.descricao}>
        <p>{enterprise.description}</p>
      </div>
    </Card>
  );
}

export default CardEmpresa;
