import React, { lazy } from "react";

const Home = lazy(() => import("./pages/Home"));
const ErrorPage = lazy(() => import("./pages/Error"));

export const routes = [
  { path: "/", exact: true, component: Home },
  {
    path: "",
    exact: true,
    component: ErrorPage
  }
];
